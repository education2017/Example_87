package hr.ferit.bruno.example_87;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Zoric on 30.8.2017..
 */

public interface WiredAPI {

    @GET("feed/rss")
    Call<Feed> getNews();
}
