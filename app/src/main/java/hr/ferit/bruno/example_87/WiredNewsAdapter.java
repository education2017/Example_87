package hr.ferit.bruno.example_87;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zoric on 31.8.2017..
 */

public class WiredNewsAdapter extends BaseAdapter {

    List<FeedItem> mNewsItems;

    public WiredNewsAdapter(List<FeedItem> newsItems) {
        mNewsItems = newsItems;
    }

    @Override
    public int getCount() {
        return this.mNewsItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mNewsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NewsItemHolder itemHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_movie, parent, false);
            itemHolder = new NewsItemHolder(convertView);
            convertView.setTag(itemHolder);
        }else {
            itemHolder = (NewsItemHolder) convertView.getTag();
        }

        FeedItem newsItem = this.mNewsItems.get(position);

        itemHolder.tvNewsTitle.setText(newsItem.getTitle());
        itemHolder.tvNewsDescription.setText(newsItem.getDescription());
        Picasso.with(parent.getContext())
                .load(newsItem.getLink().getURL())
                .fit()
                .centerCrop()
                .error(R.drawable.image_unavailable)
                .placeholder(R.drawable.image_placeholder)
                .into(itemHolder.ivNewsThumbnail);

        return convertView;
    }

    static class NewsItemHolder{
        @BindView(R.id.tvNewsTitle) TextView tvNewsTitle;
        @BindView(R.id.tvNewsdescription) TextView tvNewsDescription;
        @BindView(R.id.ivNewsThumbnail) ImageView ivNewsThumbnail;

        public NewsItemHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
