package hr.ferit.bruno.example_87;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Zoric on 31.8.2017..
 */
    /*
    <item>
			<title>Nest Thermostat E: Just Like the Original, Only Cheaper</title>
			<description>The $169 Thermostat E brings all of Nest's best features into a simpler, cheaper device.</description>
			<link>https://www.wired.com/story/nests-new-thermostat-is-just-like-the-original-only-cheaper</link>
			<guid isPermaLink="false">59a722381a8f341cb926b8a5</guid>
			<pubDate>Thu, 31 Aug 2017 04:01:00 +0000</pubDate>
			<media:content/>
			<category>Gear</category>
			<media:keywords>nest, smart home, IoT</media:keywords>
			<dc:creator>David Pierce</dc:creator>
			<media:thumbnail url="https://media.wired.com/photos/59a734f11a8f341cb926b8a7/master/pass/Nest-MainArt.jpg" width="2400" height="1800"/>
	</item>
    */

@Root(name="item", strict=false)
public class FeedItem {

    @Element(name="title") private String mTitle;
    @Element(name="description") private String mDescription;
    @Element(name="link") private String mMore;
    @Element (name = "thumbnail") private Link mUrl;

    public FeedItem() {}

    public FeedItem(String title, String description, String more, Link url) {
        mTitle = title;
        mDescription = description;
        mMore = more;
        mUrl = url;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getMore() {
        return mMore;
    }

    public Link getLink() {
        return mUrl;
    }
}
