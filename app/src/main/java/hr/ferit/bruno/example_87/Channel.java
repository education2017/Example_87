package hr.ferit.bruno.example_87;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Zoric on 31.8.2017..
 */

@Root(name="channel", strict = false)
public class Channel {
    @ElementList(inline = true, name = "item") private List<FeedItem> mItems;

    public Channel() {}

    public Channel(List<FeedItem> items) {
        mItems = items;
    }

    public List<FeedItem> getItems() {
        return mItems;
    }
}
