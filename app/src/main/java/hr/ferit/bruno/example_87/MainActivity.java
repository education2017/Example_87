package hr.ferit.bruno.example_87;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class MainActivity extends Activity implements Callback<Feed> {

    private static final String MESSAGE_ERROR = "Sorry, an error occured";
    private static final String BASE_URL = "https://www.wired.com/";
    @BindView(R.id.lvNews) ListView lvNewsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.loadData();
    }

    private void loadData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        WiredAPI wiredAPI = retrofit.create(WiredAPI.class);
        Call<Feed> call = wiredAPI.getNews();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Feed> call, Response<Feed> response) {
        Channel feed = response.body().getChannel();
        List<FeedItem> items = feed.getItems();
        WiredNewsAdapter adapter = new WiredNewsAdapter(items);
        lvNewsList.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<Feed> call, Throwable t) {
        Log.e("Error", t.getMessage());
        Toast.makeText(this, MESSAGE_ERROR, Toast.LENGTH_SHORT).show();
    }
}
