package hr.ferit.bruno.example_87;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/**
 * Created by Zoric on 31.8.2017..
 */

@Root(name="thumbnail", strict=false)
public class Link {
    @Attribute(name="url") private String mURL;

    public Link() { }

    public Link(String URL) {
        mURL = URL;
    }

    public String getURL() {
        return mURL;
    }
}
