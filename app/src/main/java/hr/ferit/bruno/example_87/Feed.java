package hr.ferit.bruno.example_87;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * Created by Zoric on 31.8.2017..
 */

@Root(name="rss", strict = false)
class Feed {
    @Element(name="channel") Channel mChannel;

    public Feed() {
    }

    public Feed(Channel channel) {
        mChannel = channel;
    }

    public Channel getChannel() {
        return mChannel;
    }
}
